package sample.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import sample.model.Customer;
import sample.model.Ticket;

import java.util.Date;

public class EnteringCustomerController {

    @FXML
    private Button printTicket;

    @FXML
    private TextField plate_input;

    @FXML
    private Label plate_error;

    @FXML
    private Label no_parking_available;
    @FXML
    private Label slot_num;
    @FXML
    private Label plate_num;
    @FXML
    private Label create_date;

    @FXML
    private Pane ticketPan;

    private final Customer customer = new Customer();

    public void initialize() {

        printTicket.setOnMouseClicked(mouseEvent -> {
            plate_error.setVisible(false);
            no_parking_available.setVisible(false);
            ticketPan.setVisible(false);

            if (plate_input.getText().isBlank()) {
                plate_error.setVisible(true);
                return;
            }
            Ticket ticket = customer.getTicket(plate_input.getText());
            if (ticket == null) {
                no_parking_available.setVisible(true);
                ticketPan.setVisible(false);

            } else {
                no_parking_available.setVisible(false);
                ticketPan.setVisible(true);
                slot_num.setText("Slot " + ticket.getSlot());
                plate_num.setText(ticket.getPlate());
                create_date.setText("date " + new Date().toString());

            }


        });

    }

}
