package sample.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import sample.Main;
import sample.model.Person;

import java.io.IOException;

public class LoginController {

    @FXML
    private Button login;

    @FXML
    private Button parking_login;

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private Label error;

    private final Person person = new Person();

    public void initialize() {
        parking_login.setOnMouseClicked(mouseEvent -> {
            try {
                Main.changeSeen(FXMLLoader.load(getClass().getResource("/sample/ui/entring.fxml")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        login.setOnMouseClicked(mouseEvent -> {
            error.setText("");
            if (username.getText().isBlank()) {
                error.setText("please enter username");
                return;
            }

            if (password.getText().isBlank()) {
                error.setText("please enter password");
                return;
            }

            try {

                switch (person.login(username.getText(), password.getText())) {
                    case ADMIN -> {
                        Main.changeSeen(FXMLLoader.load(getClass().getResource("/sample/ui/admin.fxml")));
                    }
                    case ENTRY_OPERATOR -> {
                        Main.changeSeen(FXMLLoader.load(getClass().getResource("/sample/ui/entringOprator.fxml")));
                    }
                    case EXIT_OPERATOR -> {
                        Main.changeSeen(FXMLLoader.load(getClass().getResource("/sample/ui/exitOprator.fxml")));
                    }
                    default -> {
                        error.setText("username or password is wrong");
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("button clicked " + username.getText() + " -- " + password.getText());
        });

    }


}
