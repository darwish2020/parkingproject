package sample.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import sample.model.Admin;
import sample.model.Person;
import sample.model.UserType;

public class AdminController {

    @FXML
    private Button delete;

    @FXML
    private ListView<Person> usersList;
    @FXML
    private TextField name_field;
    @FXML
    private TextField username_field;
    @FXML
    private PasswordField password_field;
    @FXML
    private ComboBox<UserType> type_spinner;

    @FXML
    private Button add_new_user;

    @FXML
    private Label error_adding;

    @FXML
    private Tab user_tab;

    private final Admin admin = new Admin();

    private void updateUserList() {
        usersList.getItems().clear();
        usersList.getItems().addAll(admin.showAllUsers());
    }

    public void initialize() {

        updateUserList();

        usersList.setCellFactory(param -> new ListCell<Person>() {
            @Override
            protected void updateItem(Person person, boolean empty) {
                super.updateItem(person, empty);
                if (empty || person == null || person.getName() == null) {
                    setText(null);
                } else {
                    setText(person.getName());
                }


            }
        });


        type_spinner.getItems().clear();
        type_spinner.getItems().add(UserType.ADMIN);
        type_spinner.getItems().add(UserType.ENTRY_OPERATOR);
        type_spinner.getItems().add(UserType.EXIT_OPERATOR);

        type_spinner.getSelectionModel().select(0);

        type_spinner.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(UserType userType, boolean empty) {
                super.updateItem(userType, empty);

                if (empty || userType == null) {
                    setText(null);
                } else {
                    setText(userType.name());
                }
            }
        });

        delete.setOnMouseClicked(mouseEvent -> {
            ObservableList<Person> selectUser = usersList.getSelectionModel().getSelectedItems();
            for (Person s : selectUser)
                admin.deleteUser(s.getUsername());
            updateUserList();
        });

        add_new_user.setOnMouseClicked(mouseEvent -> {
            if (name_field.getText().isBlank()) {
                error_adding.setText("please enter name");
                return;
            }
            if (username_field.getText().isBlank()) {
                error_adding.setText("please enter username");
                return;
            }

            if (password_field.getText().isBlank()) {
                error_adding.setText("please enter password");
                return;
            }

            if (admin.addNewUser(new Person(name_field.getText(), username_field.getText(), password_field.getText(), type_spinner.getSelectionModel().getSelectedItem()))) {
                error_adding.setText("create user success");
                updateUserList();
            } else {
                error_adding.setText("user already exists");
            }
        });


    }


}
