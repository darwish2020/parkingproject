package sample.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Admin extends Person implements AdminOperation {

    @Override
    public UserType getUserType() {
        return UserType.ADMIN;
    }


    @Override
    public boolean addNewUser(Person person) {
        try {
            File file = new File(Config.userFile);
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                String[] userdata = data.split(",");
                if (userdata[1].equalsIgnoreCase(person.getUsername())) {
                    reader.close();
                    return false;
                }
            }
            reader.close();


            FileWriter writer = new FileWriter(file, true);
            String data = person.getName() + "," + person.getUsername() + "," + person.getPassword() + "," + person.getUserType().name() + "\n";
            writer.append(data);
            writer.flush();
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }


    }


    @Override
    public boolean updateUser(Person person) {
        return false;
    }

    @Override
    public boolean deleteUser(String person) {
        try {
            File file = new File(Config.userFile);
            Scanner reader = new Scanner(file);
            String temp = "";
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                String[] userdata = data.split(",");
                if (!userdata[1].equalsIgnoreCase(person)) {
                    temp = temp.concat(data+"\n");
                }
            }
            reader.close();
            FileWriter writer = new FileWriter(file);
            writer.append(temp);
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();

        }

        return true;
    }

    @Override
    public List<Person> showAllUsers() {
        ArrayList<Person> users = new ArrayList<>();
        try {
            File file = new File(Config.userFile);
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                System.out.println(data);
                String[] userdata = data.split(",");
                users.add(new Person(
                        userdata[0],
                        userdata[1],
                        userdata[2],
                        UserType.valueOf(userdata[3])

                ));
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        return users;
    }

    @Override
    public void SetMaxSlot(int slot) {

    }

    @Override
    public void updateSlot(int slot) {

    }
}
