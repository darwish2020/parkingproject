package sample.model;

import java.util.Date;

public class Ticket {
    private String plate;
    private int slot;
    private Date createAt;

    public Ticket(String plate, int slot, Date createAt) {
        this.plate = plate;
        this.slot = slot;
        this.createAt = createAt;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Ticket(String plate, int slot) {
        this.plate = plate;
        this.slot = slot;
    }

    public Ticket() {
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
