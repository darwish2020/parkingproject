package sample.model;

import java.util.Date;

public class Parking {
    private static Parking parking = null;

    public static Parking getInstance() {
        if (parking == null)
            parking = new Parking();
        return parking;
    }


    private int maxSlot = 10;
    private float hourPrice=2;

    public int getMaxSlot() {
        return maxSlot;
    }

    public void setMaxSlot(int maxSlot) {
        this.maxSlot = maxSlot;
    }

    public float getHourPrice() {
        return hourPrice;
    }

    public void setHourPrice(float hourPrice) {
        this.hourPrice = hourPrice;
    }

    private Ticket[] tickets = new Ticket[maxSlot];

    public Ticket addTicket(String plate) {
        for (int i = 0; i < tickets.length; i++) {
            if (tickets[i] == null) {
                tickets[i] = new Ticket(plate, i+1, new Date());
                return tickets[i];
            }
        }
        return null;
    }

    public Ticket removeTicket(int index){
        Ticket ticket=tickets[index-1];
        tickets[index-1]=null;
        return ticket;
    }



}
