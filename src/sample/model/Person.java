package sample.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Person {
    private String name, username, password;
    private UserType userType;

    public Person() {
    }

    public Person(String name, String username, String password, UserType userType) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public UserType login(String username, String password) {
        try {
            File file = new File(Config.userFile);
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                System.out.println(data);
                String[] userdata = data.split(",");
                if (userdata[1].equalsIgnoreCase(username) && userdata[2].equalsIgnoreCase(password)) {
                    return UserType.valueOf(userdata[3]);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        }
        return UserType.OTHER;

    }
}
