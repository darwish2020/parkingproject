package sample.model;

import java.util.List;

public interface AdminOperation {
    boolean addNewUser(Person person);

    boolean updateUser(Person person);

    boolean deleteUser(String person);

    List<Person> showAllUsers();


    void SetMaxSlot(int slot);

    void updateSlot(int slot);




}
