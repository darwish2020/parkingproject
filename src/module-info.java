module JavaFxApplication {
    requires javafx.graphics;
    requires javafx.controls;
    requires javafx.fxml;

    opens sample;
    opens sample.controller;
    opens sample.ui;
    opens sample.model;

}